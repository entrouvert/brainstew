$(function() {
  var search_button = $('<button id="search-button">/</button>');
  var search_input = $('<input id="search-input" type="search"></input>');
  var search_results = $('<div id="search-results"><ul><li><a href="#">sdsdsd </a></li></ul></div>');
  var search_index = null;
  var pages = Object();
  search_results.prependTo($('div.header'));
  search_input.prependTo($('div.header'));
  search_button.prependTo($('div.header'));
  $(search_button).click(function() {
    search_input.toggle();
    if ($('#search-input:visible').length == 0) {
      search_results.hide();
      search_input.val('');
    } else {
      search_input.focus();
    }
    $('.body').toggleClass('search-dim');
    if (search_index === null) {
      $('<script src="../../lunr.min.js"></script>').appendTo('body');
      $('<script src="../../lunr.stemmer.support.min.js"></script>').appendTo('body');
      $('<script src="../../lunr.fr.min.js"></script>').appendTo('body');
      search_index = lunr(function () {
          this.use(lunr.fr);
          this.field('title', {boost: 10})
          this.field('other_titles', {boost: 5})
          this.field('text')
          this.ref('id')
      });
      $.ajax({
        type: 'GET',
        url: 'pages.json',
        success: function(data) {
          $(data).each(function(index, elem) {
            pages[elem.id] = elem.title;
            search_index.add(elem);
          });
        },
        dataType: 'json',
        async: true
      });
    }
  });
  $(search_input).keyup(function() {
    var search_txt = $(search_input).val();
    $('#search-results').show();
    $('#search-results ul').empty();
    if (search_txt.length > 3 && search_index) {
      console.log('search txt:', search_txt);
      var results = search_index.search(search_txt);
      $(results).each(function(index, elem) {
              console.log(elem);
        $('<li><a href="' + elem.ref + '.html">' + pages[elem.ref] + '</li>').appendTo('#search-results ul');
      });
      console.log(results);
    }
  });
});
