<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template name="html.head.custom">
    <link rel="stylesheet" type="text/css" href="brainstew.css"/>
    <script type="text/javascript" src="brainstew.js"></script>
  </xsl:template>
</xsl:stylesheet>
