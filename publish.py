#! /usr/bin/env python3

import json
import os
import shutil
import subprocess
import re

MAL_NS = 'http://projectmallard.org/1.0/'

jhbuildrc = os.path.join(os.getcwd(), 'jhbuildrc')
checkouts_directory = os.path.join(os.getcwd(), '..', 'src')
web_directory = os.path.join(os.getcwd(), '..', 'web')

os.environ['JHBUILD_RUN_AS_ROOT'] = '1'

if not os.path.exists('jhbuild'):
    os.system('jhbuild  -f jhbuildrc -m doceo.modules buildone itstool yelp-xsl yelp-tools')

if not os.path.exists(checkouts_directory):
    os.mkdir(checkouts_directory)


def checkout(module):
    checkout_dir = os.path.join(checkouts_directory, module)

    kws = {}
    kws['stdout'] = subprocess.PIPE
    kws['stderr'] = subprocess.STDOUT
    if not os.path.exists(checkout_dir):
        # full clone
        cmd = ['git', 'clone', 'https://git.entrouvert.org/entrouvert/%s.git' % module]
        kws['cwd'] = checkouts_directory
    else:
        # pull
        cmd = ['git', 'pull']
        kws['cwd'] = checkout_dir

    subprocess.call(cmd, **kws)


def publish_mallard(module, branch, directory):
    module_name = module.get('name')
    checkout_dir = os.path.join(checkouts_directory, module_name)

    kws = {}
    kws['stdout'] = subprocess.PIPE
    kws['stderr'] = subprocess.STDOUT
    cmd = ['git', 'checkout', branch]
    subprocess.call(cmd, cwd=checkout_dir, **kws)

    help_dir = os.path.join(checkout_dir, 'help', 'fr')
    if not os.path.exists(help_dir):
        return
    output_dir = os.path.join(web_directory, module_name, directory)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    index_html = os.path.join(output_dir, 'index.html')
    if os.path.islink(index_html):
        os.unlink(index_html)

    page_files = [os.path.join(help_dir, x) for x in os.listdir(help_dir) if x.endswith('.page')]
    cmd = ['jhbuild', '-f', jhbuildrc, 'run', 'yelp-build', 'html',
            '-o', output_dir, '-x']
    if module.get('branch') == 'dev' and module.get('private'):
        cmd.append('mallard_extra_dev.xsl')
    else:
        cmd.append('mallard_extra.xsl')
    cmd.extend(page_files)
    subprocess.call(cmd, **kws)
    for static in ('brainstew.js', 'brainstew.css'):
        shutil.copy(static, output_dir)

    if not os.path.exists(index_html):
        html_files = [x for x in os.listdir(output_dir) if x.endswith('.html')]
        if html_files:
            os.symlink(html_files[0], index_html)

    def remove_tags(string):
        return re.sub('<.*?>', '', string, flags=re.DOTALL)

    pages = []
    for page_file in page_files:
        text = open(page_file).read()
        text = re.sub(r'<title.*type="sort".*</title>', '', text)
        titles = re.findall(r'<title.*>(.*)<\/title>', text)
        if not titles:
            continue
        pages.append({
            'id': os.path.basename(page_file).rsplit('.', 1)[0],
            'title': remove_tags(titles[0]),
            'other_titles': remove_tags(' '.join(titles[1:])),
            'text': remove_tags(text),
            })

    json.dump(pages, open(os.path.join(output_dir, 'pages.json'), 'w'), indent=2)


with open('modules.json') as fd:
    modules = json.load(fd)

for module in modules:
    if module.get('type') in ('sphinx', 'tarball'):
        continue
    checkout(module.get('name'))
    module['branch'] = 'dev'
    publish_mallard(module, 'main', 'dev')

for js_file in os.listdir('js'):
    shutil.copy('js/' + js_file, web_directory)
